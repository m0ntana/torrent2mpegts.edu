TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    torrenthandle.cpp \
    torrentsession.cpp \
    storagememcache.cpp \
    filestreamer.cpp \
    controlinterface.cpp

LIBS += -ltorrent-rasterbar -lpthread -lboost_system -lboost_chrono -lboost_thread -lcrypto

HEADERS += \
    torrenthandle.h \
    torrentsession.h \
    storagememcache.h \
    filestreamer.h \
    controlinterface.h
