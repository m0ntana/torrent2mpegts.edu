#include "filestreamer.h"

FileStreamer::FileStreamer(const libtorrent::file_storage &storage, int file_index)
{
    this->index = file_index;
    this->m_name = storage.file_name(this->index);
    this->m_hash = this->md5_hash(this->m_name);

    this->m_size = storage.file_size(this->index);
    this->offset = storage.file_offset(this->index);

    this->piece_info.first = storage.map_file(this->index, 0, 0).piece;
    this->piece_info.last = storage.map_file(this->index, this->m_size - 1, 0).piece;
    this->piece_info.count = this->piece_info.last - this->piece_info.first;

    this->piece_info.size = storage.piece_length();
    this->piece_info.first_size = storage.piece_size(this->piece_info.first);
    this->piece_info.last_size = storage.piece_size(this->piece_info.last);

    this->piece_info.first_byteoffset = this->offset - this->piece_info.first * this->piece_info.size;
    this->piece_info.last_lastbyte = (this->offset + this->m_size) - this->piece_info.last * this->piece_info.size;

    this->piece_info.last_written = this->piece_info.first - 1;
    this->piece_info.last_prioritized = -1;

    this->m_bytes_done = 0;
    this->m_pieces_done = 0;

    std::cerr << "CONSTRUCTOR: FileStreamer()\n";
}

FileStreamer::~FileStreamer()
{
    std::cerr << "DESTRUCTOR: FileStreamer()\n";
    this->close_stream();
}

void FileStreamer::set_ffmpeg_script(std::string s)
{
    m_ffmpeg_script = s;
    std::cerr << "INFO: ffmpeg_script: " << m_ffmpeg_script << std::endl;
}

void FileStreamer::set_ts_output_dir(std::string s)
{
    m_ts_output_dir = s;
    std::cerr << "INFO: ts_outputdir: " << m_ts_output_dir << std::endl;
}

bool FileStreamer::open_stream()
{
    std::string default_cmd("cat > /dev/null");
    std::string cmd;
    if(m_ffmpeg_script.size() < 1 || m_ts_output_dir.size() < 1) {
        std::cerr << "WARN: No ffmpeg_script or ts_output_dir specified\n";
        std::cerr << "WARN: default command: '" << default_cmd << "'\n";

        cmd = default_cmd;
    }
    else {
        cmd = m_ffmpeg_script + " " + m_ts_output_dir + "/" + this->m_hash;
    }

    //std::string filename = this->m_name + std::string(".output");
    //std::string filename = "/dev/null";
    //this->m_output_file = fopen(filename.c_str(), "w");

    std::cerr << "DBG: popen(): " << cmd << std::endl;
    this->m_output_file = popen(cmd.c_str(), "w");

    return true;
}

bool FileStreamer::close_stream()
{
    if(this->m_output_file == NULL)
        return true;
    fclose(this->m_output_file);
    this->m_output_file = NULL;

    return true;
}

bool FileStreamer::push(int piece_index, char *buf, int buf_size)
{
    bool pushed = this->_push(piece_index, buf, buf_size);
    if(this->is_done())
        this->close_stream();

    if(pushed) {
        this->m_bytes_done += buf_size;
        this->m_pieces_done += 1;
    }

    return pushed;
}

bool FileStreamer::is_piece_in_range(int piece_index)
{
    return (piece_index >= this->piece_info.first && piece_index <= this->piece_info.last);
}

int FileStreamer::get_valid_piece_size(int piece_index)
{
    if(piece_index == this->piece_info.first)
        return this->piece_info.first_size;
    else if(piece_index == this->piece_info.last)
        return this->piece_info.last_size;
    else
        return this->piece_info.size;
}

bool FileStreamer::get_pieces_to_prioritize(std::vector<int> &p)
{
    if(this->is_done())
        return false;

    if(this->piece_info.last_prioritized == (this->piece_info.last_written + 1))
        return false;

    this->piece_info.last_prioritized = this->piece_info.last_written + 1;

    for(int i = this->piece_info.last_prioritized; i < this->piece_info.last_prioritized + 10; i++) {
        if(i >= this->piece_info.last)
            break;
        p.push_back(i);
    }

    return true;
}

bool FileStreamer::is_done()
{
    return (this->piece_info.last_written >= this->piece_info.last);
}

int FileStreamer::next_piece()
{
    return this->piece_info.last_written + 1;
}


void FileStreamer::print_info()
{
    std::cerr << "FILE INFO index " << this->index <<
                 "\nName: " << this->m_name <<
                 "\nHash: " << this->m_hash <<
                 "\nSize: " << this->m_size << "; offset: " << this->offset <<
                 "\nPiece range: " << this->piece_info.first << " - " << this->piece_info.last <<
                 "\nPiece first/last size: " << this->piece_info.first_size << "/" << this->piece_info.last_size <<
                 "\nPiece first/last byte offset: " << this->piece_info.first_byteoffset << "/" << this->piece_info.last_lastbyte <<
                 std::endl;

}

std::string FileStreamer::name()
{
    return this->m_name;
}

std::string FileStreamer::hash()
{
    return this->m_hash;
}

std::string FileStreamer::type()
{
    std::size_t idx = this->m_name.find_last_of(".");
    if(idx == std::string::npos)
        return std::string();

    return this->m_name.substr(idx + 1);
}

float FileStreamer::done_ratio()
{
    if(this->m_bytes_done <= 0)
        return (float)0;
    return (100 / (float)(this->m_size / this->m_bytes_done) / 100);
}

std::int64_t FileStreamer::bytes_done()
{
    return m_bytes_done;
}

int FileStreamer::pieces_done()
{
    return m_pieces_done;
}

int64_t FileStreamer::size()
{
    return this->m_size;
}

std::string FileStreamer::hash_to_string(libtorrent::sha1_hash hash)
{
    std::stringstream ss;

    for(int i = 0; i < hash.size; i ++)
        ss << std::hex << (int)hash[i];

    return ss.str();
}

bool FileStreamer::_push(int piece_index, char *buf, int buf_size)
{
    if(!this->m_output_file)
        return false;

    char *data_p;
    int len = 0;
    if(piece_index == this->piece_info.first) {
        data_p = buf + this->piece_info.first_byteoffset;
        len = buf_size - this->piece_info.first_byteoffset;
    }
    else if(piece_index == this->piece_info.last) {
        data_p = buf;
        len = this->piece_info.last_lastbyte;
    }
    else {
        data_p = buf;
        len = buf_size;
    }

    if(data_p == NULL)
        std::cerr << "ERR: FileStreamer::push() data_p is NULL!\n";
    else {
        fwrite(data_p, len, 1, this->m_output_file);
        fflush(this->m_output_file);
    }

    this->piece_info.last_written = piece_index;

    return true;
}

std::string FileStreamer::md5_hash(std::string input)
{
    std::stringstream ss;
    unsigned char raw_hash[MD5_DIGEST_LENGTH];

    MD5((const unsigned char*)input.c_str(), input.size(), raw_hash);

    for(int i = 0; i < MD5_DIGEST_LENGTH; i++)
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)raw_hash[i];

    return ss.str();
}
