#-------------------------------------------------
#
# Project created by QtCreator 2021-01-24T19:37:05
#
#-------------------------------------------------

QT       -= core gui

TARGET = torrent2mpegts
TEMPLATE = lib

DEFINES += TORRENT2PIPE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    filestreamer.cpp \
    storagememcache.cpp \
    torrenthandle.cpp \
    torrentsession.cpp \
    pythonwrapper.cpp \
    controlinterface.cpp

HEADERS += \
        torrent2pipe_global.h \ 
    filestreamer.h \
    storagememcache.h \
    torrenthandle.h \
    torrentsession.h \
    controlinterface.h

LIBS += -ltorrent-rasterbar -lpthread -lboost_system -lboost_chrono -lboost_thread

QMAKE_CXXFLAGS += -std=c++11

unix {
    target.path = /usr/lib
    INSTALLS += target
}
