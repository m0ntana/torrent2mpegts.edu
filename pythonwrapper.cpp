#include "torrentsession.h"

int _TS_string_to_buffer(std::string input, char *output, int max_len) {
    if(input.size() > (long unsigned int)max_len)
        return input.size();
    std::memcpy(output, input.c_str(), input.size());

    return 0;
}

extern "C" {
TorrentSession *SessionInstance;

TorrentSession* TS_new() {
    return new TorrentSession();
}

// params: json string
void TS_run_loop(char *params) {
    SessionInstance = new TorrentSession();
    SessionInstance->create(std::string(params));
    SessionInstance->run();
}

void TS_run(TorrentSession *instance) {
    instance->run();
}

TorrentHandle* TS_add_magnet(char *input) {
    TorrentHandle *h = SessionInstance->add_magnet(std::string(input));
    return h;
}

TorrentHandle* TS_add_file(char *input) {
    TorrentHandle *h = SessionInstance->add_file(std::string(input));
    return h;
}

int TS_has_metadata(TorrentHandle *h) {
    if(!SessionInstance->t_handle_valid(h)) {
        std::cerr << "ERR: invalid handle\n";
        return -1;
    }

    return ((h->has_metadata())? 1 : 0);
}

int TS_get_metadata(TorrentHandle *h, char *buf, int buf_len) {
    if(!SessionInstance->t_handle_valid(h)) {
        std::cerr << "ERR: invalid handle\n";
        return -1;
    }

    std::string metadata = h->json_metadata();
    if(buf_len == 0 || buf_len < metadata.size())
        return metadata.size();

    memcpy(buf, metadata.c_str(), metadata.size());

    return 0;
}

int TS_stop_torrent(TorrentHandle *h) {
    if(!SessionInstance->t_handle_valid(h)) {
        std::cerr << "ERR: invalid handle\n";
        return -1;
    }

    return SessionInstance->stop_torent(h);
}

int TS_cb_set_stats_update(TorrentHandle *h, int(*ptr)()) {
    if(!SessionInstance->t_handle_valid(h)) {
        std::cerr << "ERR: invalid handle\n";
        return -1;
    }
    h->cb_set_stats_update(ptr);
    return 1;
}

int TS_cb_set_torrent_finished(TorrentHandle *h, int(*ptr)()) {
    if(!SessionInstance->t_handle_valid(h)) {
        std::cerr << "ERR: invalid handle\n";
        return -1;
    }
    h->cb_set_torrent_finished(ptr);
    return 1;
}

int TS_cb_set_file_finished(TorrentHandle *h, int(*ptr)()) {
    if(!SessionInstance->t_handle_valid(h)) {
        std::cerr << "ERR: invalid handle\n";
        return -1;
    }
    h->cb_set_file_finished(ptr);
    return 1;
}

}

