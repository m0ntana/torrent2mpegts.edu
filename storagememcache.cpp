#include "storagememcache.h"

std::mutex StorageMemCache::piece_data_mutex;

StorageMemCache::StorageMemCache(lt::storage_params const& p)
{
    std::cerr << "CONSTRUCTOR: StorageMemCache()\n";

    m_pieces_count = p.files->num_pieces();
    m_pieces_freed = 0;

    m_single_piece_size = p.files->piece_length();
    m_last_piece_size = p.files->piece_size(m_pieces_count - 1);

    DEBUG = false;

    piece_data = new piece_data_t[m_pieces_count];
}

StorageMemCache::~StorageMemCache()
{
    this->free_pieces();
    if((m_pieces_count - m_pieces_freed) > 0)
        std::cerr << "ERR: storage: " << (m_pieces_count - m_pieces_freed) << " pieces were not freed\n";

    delete[] piece_data;
    if(malloc_trim(0))
        std::cerr << "~StorageMemCache() malloc_trim() reported memory released\n";


    std::cerr << "DESTRUCTOR: StorageMemCache()\n";
}


int StorageMemCache::readv(const file::iovec_t *bufs, int num_bufs, int piece_index, int offset, int flags, storage_error &ec)
{
    (void) flags;
    (void) ec;

    std::lock_guard<std::mutex> g(piece_data_mutex);

    if(!this->piece_exists(piece_index))
        return 0;

    int total_size = 0;
    for(int i=0; i < num_bufs; i++) {
        const lt::file::iovec_t *buf = &bufs[i];
        piece_data_t &p = piece_data[piece_index];

        if(p.buffer_size < (offset + buf->iov_len)) {
            std::cerr << "ERR: storage::read(): piece=" << piece_index << "; offset+size=" << (offset + buf->iov_len) <<
                         " but only " << p.buffer_size << " available\n";
            break;
        }

        memcpy(buf->iov_base, p.buffer + offset, buf->iov_len);
        offset += buf->iov_len;
        total_size += buf->iov_len;
    }

    return total_size;
}

int StorageMemCache::writev(const file::iovec_t *bufs, int num_bufs, int piece_index, int offset, int flags, storage_error &ec)
{
    // just to fix warnings
    (void)flags;
    (void)ec;

    std::lock_guard<std::mutex> g(piece_data_mutex);

    if(DEBUG) std::cerr << "storage::write(): ";

    if(!this->piece_exists(piece_index)) {
        if(DEBUG) std::cerr << " new piece: " << piece_index;
    }
    else {
        if(DEBUG) std::cerr << " piece: " << piece_index;
    }

    if(DEBUG) std::cerr << "; num_bufs: " << num_bufs << " offset: " << offset;

    int piece_size = (( piece_index != (m_pieces_count - 1) )? m_single_piece_size : m_last_piece_size);
    piece_data_t &p = piece_data[piece_index];
    if(p.buffer_size < piece_size) {
        p.buffer_size = piece_size;
        p.buffer = (char*) realloc(p.buffer, p.buffer_size);
    }

    int new_full_size = offset;
    for(int i=0; i < num_bufs; i++)
        new_full_size += bufs[i].iov_len;


    if(p.buffer_size < new_full_size) {
        std::cerr << "ERR: new_full_size is bigger than single piece size?!\n";
        return 0;
    }

    int total_written = 0;
    for(int i=0; i < num_bufs; i++) {
        const lt::file::iovec_t *buf = &bufs[i];

        memcpy(p.buffer + offset, buf->iov_base, buf->iov_len);

        offset += buf->iov_len;
        total_written += buf->iov_len;
    }

    if(DEBUG)
        std::cerr << "; total_written: " << total_written <<
                     "; piece.size: " << piece_data[piece_index].buffer_size <<
                     "; size: " << offset <<
                     std::endl;

    return total_written;
}

bool StorageMemCache::piece_exists(int piece_index)
{
    return ((piece_index < m_pieces_count) && (piece_data[piece_index].buffer != NULL));
}

int StorageMemCache::get_piece_data(int piece_index, char **output_buffer)
{
    std::lock_guard<std::mutex> g(piece_data_mutex);
    if(!this->piece_exists(piece_index))
        return 0;

    piece_data_t &p = piece_data[piece_index];

    if(p.buffer == NULL) {
        std::cerr << "ERR: p.buffer is NULL!";
        return 0;
    }

    *output_buffer = (char*) realloc(*output_buffer, p.buffer_size);
    memcpy(*output_buffer, p.buffer, p.buffer_size);

    return p.buffer_size;
}

bool StorageMemCache::increment_piece_counter(int piece_index)
{
    if(!this->piece_exists(piece_index))
        return false;
    piece_data[piece_index].count ++;

    return true;
}

bool StorageMemCache::free_pieces(std::vector<int> &pieces)
{
    for(int p : pieces)
        this->piece_free(p);
    return true;
}

bool StorageMemCache::free_pieces()
{
    for(int piece_index = 0; piece_index < m_pieces_count; piece_index++) {
        if(!this->piece_free(piece_index)) {
            std::cerr << "ERR: free_pieces(all): piece " << piece_index << " NOT freed\n";
        }
    }

    return true;
}


bool StorageMemCache::piece_free(int piece_index)
{
    std::lock_guard<std::mutex> g(piece_data_mutex);

    if(!this->piece_exists(piece_index)) {
        m_pieces_freed ++;
        return true;
    }

    piece_data[piece_index].count --;
    if(piece_data[piece_index].count > 0) {
        std::cerr << "ERR: piece: " << piece_index << " not freed, p->count: " << piece_data[piece_index].count << std::endl;
        return false;
    }
    free(piece_data[piece_index].buffer);
    piece_data[piece_index].buffer = NULL;

    malloc_trim(0);

    m_pieces_freed ++;

    return true;
}

storage_interface *storage_memcache_constructor(lt::storage_params const& p)
{
    std::cerr << "DBG: creating StorageMemCache...\n";
    return new StorageMemCache(p);
}
