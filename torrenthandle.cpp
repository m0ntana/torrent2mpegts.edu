#include "torrenthandle.h"

std::mutex TorrentHandle::cb_call_mutex;
std::mutex TorrentHandle::piece_ready_mutex;

TorrentHandle::TorrentHandle(nlohmann::json &params)
{
    this->m_params = params;
    this->m_metadata_ready = false;
    this->m_num_files = -1;

    this->m_hash_string = this->hash_to_string(this->m_handle.info_hash());

    std::cerr << "CONSTRUCTOR: TorrentHandle()\n";
}

TorrentHandle::~TorrentHandle()
{
    for(FileStreamer *file : m_files) {
        this->free_file(file);
    }

    std::cerr << "DESTRUCTOR: TorrentHandle()\n";
}

void TorrentHandle::add_magnet(lt::session *session, std::string data)
{
    lt::add_torrent_params params;
    boost::system::error_code e;

    lt::parse_magnet_uri(data, params, e);
    this->prepare_add_torrent_params(params);
    this->m_handle = session->add_torrent(params);
}

void TorrentHandle::add_file(session *session, std::string data)
{
    lt::add_torrent_params params;

    //params.ti = boost::make_shared<lt::torrent_info>(data);
    params.url = data;
    this->prepare_add_torrent_params(params);

    this->m_handle = session->add_torrent(params);
}

void TorrentHandle::prepare_add_torrent_params(add_torrent_params &p)
{
    p.save_path = ".";
    p.upload_limit = 1 * 1024 * 1024;
    p.storage = &storage_memcache_constructor;
}

void TorrentHandle::piece_ready(read_piece_alert *alert)
{
    std::lock_guard<std::mutex> g(TorrentHandle::piece_ready_mutex);

    StorageMemCache* storage = (StorageMemCache*) this->m_handle.get_storage_impl();

    std::vector<int> pieces_to_free;

    for(FileStreamer* file : this->m_files) {
        if(alert != NULL && !file->is_piece_in_range(alert->piece))
            continue;

        // save piece_index we just have got
        // it is done here because:
        // multiple files torrents share pieces, so we need to save piece index same exact number of times
        // to be able to free them later correctly
        if(alert != NULL)
            storage->increment_piece_counter(alert->piece);


        int next_piece = file->next_piece();

        std::pair<int, int> w_pieces_written;
        w_pieces_written.first = next_piece;
        w_pieces_written.second = 0;

        while(true) {
            if(!storage->piece_exists(next_piece) || !this->m_handle.have_piece(next_piece))
                break;


            // get expected piece length
            // get data to buffer and length of data read
            // if they both are not equel - terminate. It is incorrect and MUST NOT happen ever
            // push data to memcache
            // free allocated buffer
            int valid_piece_len = file->get_valid_piece_size(next_piece);
            int buffer_size;
            char *buffer = NULL;

            buffer_size = storage->get_piece_data(next_piece, &buffer);
            assert(valid_piece_len == buffer_size);

            file->push(next_piece, buffer, buffer_size);
            free(buffer);
            malloc_trim(0);



            pieces_to_free.push_back(next_piece);
            next_piece ++;
            w_pieces_written.second = next_piece;
        }

        if((w_pieces_written.second - w_pieces_written.first) > 3)
            std::cerr << "DBG: " << file->name() << ": piece written: " << w_pieces_written.first << "-" << w_pieces_written.second << std::endl;
        else if((w_pieces_written.second - w_pieces_written.first) > 0 && w_pieces_written.first % 10 == 0)
            std::cerr << "DBG: " << file->name() << ": piece written: " << w_pieces_written.first << "-" << w_pieces_written.second << std::endl;


        if(file->is_done()) {
            this->cb_call_file_finished();
            this->free_file(file);
        }
        else {
            std::vector<int> next_pieces;
            if(file->get_pieces_to_prioritize(next_pieces))
                this->prioritize_pieces(next_pieces);
        }
    }

    if(pieces_to_free.size())
        this->cb_call_stats_update();
    storage->free_pieces(pieces_to_free);
}

void TorrentHandle::metadata_ready()
{
    boost::shared_ptr<const lt::torrent_info> pinfo = this->m_handle.torrent_file();
    const lt::torrent_info *info = static_cast<const lt::torrent_info*>(pinfo.get());

    if(info->info_hash() != this->m_handle.info_hash())
        return;


    std::cerr << "INFO: metadata ready\n";


    this->m_hash_string = this->hash_to_string(this->m_handle.info_hash());

    std::cerr << "INFO: torrent name: " << info->name() << std::endl;

    this->m_name = info->name();

    const lt::file_storage &storage = info->files();
    this->m_num_files = storage.num_files();
    this->m_num_pieces = storage.num_pieces();
    this->m_piece_size = storage.piece_length();

    for(int i = 0; i < m_num_files; i++) {
        FileStreamer *f = new FileStreamer(storage, i);
        f->print_info();

        if(!this->is_file_type_allowed(f->type())) {
            std::cerr << "ERR: file type: '" << f->type() << "' is not allowed\n";
            this->m_handle.file_priority(i, 0);
            delete f;
            continue;
        }

        if(this->m_params["ffmpeg_script"].size() > 0)
            f->set_ffmpeg_script(this->m_params["ffmpeg_script"]);

        if(this->m_params["ts_output_dir"].size() > 0)
            f->set_ts_output_dir(this->m_params["ts_output_dir"]);


        std::vector<int> p;
        f->get_pieces_to_prioritize(p);
        this->prioritize_pieces(p);

        f->open_stream();

        m_files.push_back(f);
    }

    this->m_metadata_ready = true;

    std::cerr << "DBG: files: " << m_num_files << "; pieces: " << m_num_pieces << "; piece len: " << m_piece_size << std::endl;
}

bool TorrentHandle::has_metadata()
{
    return this->m_metadata_ready;
}

std::string TorrentHandle::json_metadata()
{
    using json = nlohmann::json;
    json jdata;

    jdata["torrent_hash"] = this->m_hash_string;
    jdata["hash"] = this->m_hash_string;
    jdata["name"] = this->m_name;

    jdata["files"] = {};

    for(FileStreamer *file : m_files) {
        json jfile;
        jfile["name"] = file->name();
        jfile["hash"] = file->hash();
        jfile["size"] = file->size();
        jfile["pieces"] = 0;
        jfile["bytes_done"] = file->bytes_done();
        jfile["pieces_done"] = file->pieces_done();

        jdata["files"].push_back(jfile);
    }

    std::string data = jdata.dump();

    return data;
}

bool TorrentHandle::is_my_torrent_handle(torrent_handle &h)
{
    return (this->m_handle == h);
}

torrent_handle &TorrentHandle::get_torrent_handle()
{
    return this->m_handle;
}

std::string TorrentHandle::hash()
{
    return this->m_hash_string;
}

bool TorrentHandle::is_done()
{
    return (this->m_metadata_ready && m_files.size() < 1);
}

bool TorrentHandle::is_file_type_allowed(std::string his_type)
{
    if(his_type.size() < 1)
        return false;

    for(std::string type : this->m_params["allowed_file_types"]) {
        if(type == his_type)
            return true;
    }

    return false;
}

std::string TorrentHandle::hash_to_string(libtorrent::sha1_hash hash)
{
    std::stringstream ss;

    for(int i = 0; i < hash.size; i ++)
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];

    return ss.str();
}

void TorrentHandle::cb_call_stats_update()
{
    std::lock_guard<std::mutex> g(TorrentHandle::cb_call_mutex);
    if(this->f_cb_stats_update)
        this->f_cb_stats_update();
}

void TorrentHandle::cb_set_torrent_finished(int(*cb)())
{
    //std::cerr << "DBG: set torrent_finished callback\n";
    this->f_cb_torrent_finished = cb;
}

void TorrentHandle::cb_call_torrent_finished()
{
    std::lock_guard<std::mutex> g(TorrentHandle::cb_call_mutex);
    if(this->f_cb_torrent_finished) {
        //std::cerr << "DBG: call torrent_finished callback\n";
        this->f_cb_torrent_finished();
    }
}

void TorrentHandle::cb_set_file_finished(int (*cb)())
{
    std::cerr << "DBG: set file_dine callback\n";
    this->f_cb_file_finished = cb;
}

void TorrentHandle::cb_call_file_finished()
{
    std::lock_guard<std::mutex> g(TorrentHandle::cb_call_mutex);
    if(this->f_cb_file_finished) {
        std::cerr << "DBG: call file_finished callback\n";
        this->f_cb_file_finished();
    }
}

void TorrentHandle::cb_set_stats_update(int (*cb)())
{
    std::cerr << "DBG: set stats_update callback\n";
    this->f_cb_stats_update = cb;

    //this->cb_call_stats_update();
}

void TorrentHandle::prioritize_pieces(std::vector<int> &p)
{
    if(p.size() < 1)
        return;

    std::pair<int, int> k;
    k.first = p[0];

    for(int i=0; (long unsigned int)i < p.size(); i++) {
        //this->m_handle.set_piece_deadline(p[i], 500 + i, torrent_handle::alert_when_available);
        this->m_handle.set_piece_deadline(p[i], 500 + i);
        k.second = p[i];
    }

    //std::cerr << "DBG: prioritized: " << k.first << "-" << k.second << std::endl;
}

void TorrentHandle::free_file(FileStreamer *file)
{
    this->m_files.erase(std::find(m_files.begin(), m_files.end(), file));
    delete file;
}
