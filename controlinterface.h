#ifndef CONTROLINTERFACE_H
#define CONTROLINTERFACE_H

#include <string>
#include <iostream>
#include <vector>
#include <cstring>


#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <poll.h>

class ControlInterface
{
public:
    ControlInterface();
    ~ControlInterface();
    bool setup();
    bool process_events(std::string &magnet);

private:
    int m_fd_r, m_fd_w;
    std::string m_pipe_file;

    bool handle_c_error(int ret, int fail_value, std::string subj = std::string());
    std::string read_input_data();
};

#endif // CONTROLINTERFACE_H
