#ifndef STORAGEMEMCACHE_H
#define STORAGEMEMCACHE_H

#include <iostream>

#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/storage.hpp>
#include <libtorrent/allocator.hpp>
#include <libtorrent/storage_defs.hpp>
#include <libtorrent/alert_types.hpp>

#include <malloc.h>

#include <boost/system/error_code.hpp>

using namespace lt;

class StorageMemCache : public lt::storage_interface, boost::noncopyable
{
public:

    StorageMemCache(const storage_params &p);

    // lt::storage_interface overloaded methods
    virtual ~StorageMemCache();
    virtual bool has_any_file(lt::storage_error&) TORRENT_OVERRIDE { return false; }
    virtual void set_file_priority(std::vector<boost::uint8_t>&, lt::storage_error&) TORRENT_OVERRIDE {}
    virtual void rename_file(int, std::string const&, lt::storage_error&) TORRENT_OVERRIDE {}
    virtual void release_files(lt::storage_error&) TORRENT_OVERRIDE {}
    virtual void delete_files(int, lt::storage_error&) TORRENT_OVERRIDE {}
    virtual void initialize(lt::storage_error&) TORRENT_OVERRIDE {}
    virtual int move_storage(std::string const&, int, lt::storage_error&) TORRENT_OVERRIDE { return 0; }

    virtual int readv(lt::file::iovec_t const* bufs, int num_bufs, int piece_index, int offset, int flags, lt::storage_error& ec) TORRENT_OVERRIDE;

    virtual int writev(lt::file::iovec_t const* bufs, int num_bufs, int piece_index, int offset, int flags, lt::storage_error& ec) TORRENT_OVERRIDE;


    virtual bool verify_resume_data(lt::bdecode_node const&
                                    , std::vector<std::string> const*
                                    , lt::storage_error&) TORRENT_OVERRIDE { return false; }
    virtual void write_resume_data(lt::entry&, lt::storage_error&) const TORRENT_OVERRIDE {}
    // -------------------


    bool piece_exists(int piece_index);
    bool increment_piece_counter(int piece_index);
    bool free_pieces(std::vector<int> &pieces);
    bool free_pieces();
    bool piece_free(int piece_index);

    int get_piece_data(int piece_index, char **output_buffer);
private:
    bool DEBUG;

    struct piece_data_t {
        int count = 0;
        int buffer_size = 0;
        char *buffer = NULL;
    };



    //std::map<int, piece_data_t*> piece_data;
    piece_data_t *piece_data;
    int m_pieces_count, m_pieces_freed, m_single_piece_size, m_last_piece_size;


    static std::mutex piece_data_mutex;
};

storage_interface* storage_memcache_constructor(storage_params const& p);


#endif // STORAGEMEMCACHE_H
