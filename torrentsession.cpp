#include "torrentsession.h"

TorrentSession* Instance = NULL;

TorrentSession::TorrentSession()
{
}

TorrentSession::~TorrentSession()
{
    std::cerr << "DESTRUCTOR: TorrentSession()\n";
    delete this->m_control_iface;
    delete this->m_session;
}

TorrentHandle* TorrentSession::add_magnet(std::string data)
{
    TorrentHandle *h = new TorrentHandle(this->m_params);
    h->add_magnet(this->m_session, data);
    t_handle_add(h);

    return h;
}

TorrentHandle *TorrentSession::add_file(std::string data)
{
    TorrentHandle *h = new TorrentHandle(this->m_params);
    h->add_file(this->m_session, data);
    t_handle_add(h);

    return h;
}

nlohmann::json &TorrentSession::get_params()
{
    return this->m_params;
}

void TorrentSession::run()
{
    if(!this->m_session) {
        std::cerr << "ERR: use TorrentSession::create() first\n";
        return;
    }

    while(true) {
        std::vector<lt::alert*> alerts;
        this->m_session->pop_alerts(&alerts);

        if(this->no_alerts(alerts))
            continue;

        for(lt::alert* general_alert : alerts)
            this->parse_alert(general_alert);
    }
}

void TorrentSession::create(std::string params)
{
    try {
        this->m_params = nlohmann::json::parse(params);
    } catch (...) {
        std::cerr << "WARN: no settings found in TorrentSession::create()\n";
    }


    if(!this->m_params["dht_bootstrap_nodes"].size())
        this->m_params["dht_bootstrap_nodes"] = "10.10.20.1:8999";
//                                                "router.utorrent.com:6881,router.bittorrent.com:8991,dht.transmissionbt.com:6881,"
//                                                "router.bitcomet.com:6881,dht.aelitis.com:6881";


    lt::settings_pack settings;
    if(this->m_params["listen_interface"].size())
        settings.set_str(settings.listen_interfaces, this->m_params["listen_interface"]);

    settings.set_int(lt::settings_pack::alert_mask, lt::alert::piece_progress_notification | lt::alert::status_notification);

    std::cerr << "INFO: DHT boostrap nodes: " << this->m_params["dht_bootstrap_nodes"] << std::endl;
    settings.set_str(settings.dht_bootstrap_nodes, this->m_params["dht_bootstrap_nodes"]);

    settings.set_bool(settings.enable_dht, true);
    settings.set_bool(settings.enable_lsd, true);
    settings.set_bool(settings.enable_natpmp, true);
    settings.set_bool(settings.enable_upnp, true);
    settings.set_bool(settings.enable_outgoing_tcp, true);
    settings.set_bool(settings.enable_outgoing_utp, true);

    settings.set_int(settings.cache_size, 0);
    settings.set_int(settings.cache_buffer_chunk_size, 0);
    settings.set_int(settings.cache_expiry, 0);
    settings.set_bool(settings.use_read_cache, true);

    settings.set_int(settings.max_peerlist_size, 500);
    settings.set_int(settings.file_pool_size, 1);

    settings.set_int(settings.active_downloads, 30);
    settings.set_int(settings.active_seeds, 2);
    settings.set_int(settings.active_checking, 10);
    settings.set_int(settings.active_limit, 100);
    settings.set_int(settings.connections_limit, 16000);
    settings.set_int(settings.torrent_connect_boost, 255);


    this->m_session = new lt::session(settings);
    this->m_control_iface = new ControlInterface();
    this->m_control_iface->setup();
}

bool TorrentSession::stop_torent(TorrentHandle *h)
{
    if(!this->t_handle_valid(h))
        return false;

    std::cerr << "DBG: stopping torrent\n";
    this->m_session->remove_torrent(h->get_torrent_handle());

    return true;
}

void TorrentSession::t_handle_add(TorrentHandle *h)
{
    std::lock_guard<std::mutex> g(m_torrent_handler_mutex);
    std::cerr << "t_handle_add()\n";
    this->m_torrent_handlers.push_back(h);
}

TorrentHandle* TorrentSession::t_handle_find(torrent_handle &original_torrent_handle)
{
    std::lock_guard<std::mutex> g(m_torrent_handler_mutex);

    for(TorrentHandle *th : m_torrent_handlers) {
        if(th->is_my_torrent_handle(original_torrent_handle)) {
            return th;
        }
    }

    std::cerr << "ERR: parent handle not found\n";
    return NULL;
}

bool TorrentSession::t_handle_valid(TorrentHandle *h)
{
    std::lock_guard<std::mutex> g(m_torrent_handler_mutex);
    return (std::find(m_torrent_handlers.begin(), m_torrent_handlers.end(), h) != m_torrent_handlers.end());
}


void TorrentSession::t_handle_remove(TorrentHandle *h)
{
    std::lock_guard<std::mutex> g(m_torrent_handler_mutex);
    std::vector<TorrentHandle*>::iterator pos = std::find(m_torrent_handlers.begin(), m_torrent_handlers.end(), h);

    if(pos == m_torrent_handlers.end()) {
        std::cerr << "ERR: can't find torrent in handlers list\n";
        return;
    }

    m_torrent_handlers.erase(pos);
    delete h;
    malloc_trim(0);
}

bool TorrentSession::no_alerts(std::vector<alert *> alerts)
{
    std::string new_magnet_link;
    if(this->m_control_iface->process_events(new_magnet_link)) {
        std::cerr << "ADD NEW MAGNET: " << new_magnet_link << std::endl;
        this->add_magnet(new_magnet_link);
    }

    if(alerts.size() < 1 && this->m_session->wait_for_alert(std::chrono::microseconds(1000000)) == NULL)
        return true;

    return false;
}

void TorrentSession::parse_alert(alert *general_alert)
{
    switch(general_alert->type()) {
    case lt::piece_finished_alert::alert_type:
        this->alert_piece_finished(static_cast<lt::piece_finished_alert*>(general_alert));
        break;
    case lt::read_piece_alert::alert_type:
        this->alert_piece_read(static_cast<lt::read_piece_alert*>(general_alert));
        break;
    case lt::metadata_received_alert::alert_type:
        this->alert_metadata_received(static_cast<lt::metadata_received_alert*>(general_alert));
        break;
    case lt::torrent_finished_alert::alert_type:
        this->alert_torrent_finished(static_cast<lt::torrent_finished_alert*>(general_alert));
        break;
    case lt::torrent_paused_alert::alert_type:
        this->alert_torrent_paused(static_cast<lt::torrent_paused_alert*>(general_alert));
        break;
    case lt::torrent_removed_alert::alert_type:
        this->alert_torrent_removed(static_cast<lt::torrent_removed_alert*>(general_alert));
        break;
    case lt::torrent_added_alert::alert_type:
        this->alert_torrent_added(static_cast<lt::torrent_added_alert*>(general_alert));
        break;
    default:
        std::cerr << "ALERT: " << general_alert->message() << std::endl;
        break;
    }
}

void TorrentSession::alert_piece_finished(piece_finished_alert *alert)
{
    alert->handle.read_piece(alert->piece_index);
}

void TorrentSession::alert_torrent_finished(torrent_finished_alert *alert)
{
    TorrentHandle *th = this->t_handle_find(alert->handle);
    if(th != NULL)
        th->piece_ready(NULL);

    th->cb_call_torrent_finished();

    alert->handle.pause();
}

void TorrentSession::alert_torrent_paused(torrent_paused_alert *alert)
{
    this->m_session->remove_torrent(alert->handle);
}

void TorrentSession::alert_torrent_removed(torrent_removed_alert *alert)
{
    TorrentHandle *th = this->t_handle_find(alert->handle);
    if(th != NULL)
        this->t_handle_remove(th);
}

void TorrentSession::alert_piece_read(read_piece_alert *alert)
{
    TorrentHandle *th = this->t_handle_find(alert->handle);
    if(th != NULL)
        th->piece_ready(alert);
}

void TorrentSession::alert_metadata_received(metadata_received_alert *alert)
{
    TorrentHandle *th = this->t_handle_find(alert->handle);
    if(th != NULL)
        th->metadata_ready();
}

void TorrentSession::alert_torrent_added(torrent_added_alert *alert)
{
    if(!alert->handle.info_hash().is_all_zeros())
        return;

    this->m_session->remove_torrent(alert->handle);
}

