#ifndef FILESTREAMER_H
#define FILESTREAMER_H

#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/torrent_info.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/storage.hpp>
#include <libtorrent/allocator.hpp>
#include <libtorrent/storage_defs.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/file_storage.hpp>

#include <openssl/md5.h>

#include <string>

class FileStreamer
{
public:
    FileStreamer(const libtorrent::file_storage &storage, int file_index);
    ~FileStreamer();
    void set_ffmpeg_script(std::string s);
    void set_ts_output_dir(std::string s);
    void print_info();

    bool open_stream();
    bool close_stream();
    bool push(int piece_index, char *buf, int buf_size);
    bool is_piece_in_range(int piece_index);
    bool get_pieces_to_prioritize(std::vector<int> &p);
    bool is_done();

    int get_valid_piece_size(int piece_index);
    int next_piece();
    int pieces_done();

    std::string name();
    std::string hash();
    std::string type();

    float done_ratio();

    std::int64_t bytes_done();
    std::int64_t size();

private:

    std::string hash_to_string(libtorrent::sha1_hash hash);
    std::string md5_hash(std::string input);

    bool _push(int piece_index, char *buf, int buf_size);


    struct piece_info_t {
        int first, last, count;
        int first_size, last_size, size;
        int first_byteoffset, last_lastbyte;
        int last_written, last_prioritized;
    };

    int index;
    std::string m_name, m_hash;
    std::int64_t m_size;
    std::int64_t offset;
    piece_info_t piece_info;

    std::int64_t m_bytes_done;
    int m_pieces_done;

    std::string m_ffmpeg_script;
    std::string m_ts_output_dir;

    FILE *m_output_file;
};

#endif // FILESTREAMER_H
