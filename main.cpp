#include <iostream>

#include "torrentsession.h"
#include <unistd.h>

#include <openssl/md5.h>

using namespace std;

int main(int argc, char const* argv[])
{
    TorrentSession *tsession = new TorrentSession();
    tsession->create();
    tsession->run();
}
