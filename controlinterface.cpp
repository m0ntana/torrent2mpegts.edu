#include "controlinterface.h"

ControlInterface::ControlInterface()
{
    m_pipe_file = "/tmp/torrent2mpegts";
}

ControlInterface::~ControlInterface()
{
    std::cerr << "DESTRUCTOR: ControlInterface()\n";
    close(m_fd_r);
}

bool ControlInterface::setup()
{
    remove(m_pipe_file.c_str());
    int ret = mkfifo(m_pipe_file.c_str(), 0666);
    if(this->handle_c_error(ret, -1, "mkfifo()")) return false;

    m_fd_r = open(m_pipe_file.c_str(), O_RDONLY|O_NONBLOCK);
    if(this->handle_c_error(m_fd_r, -1, "open(r)")) return false;

//    m_fd_w = open(m_pipe_file.c_str(), O_WRONLY|O_NONBLOCK);
//    if(this->handle_c_error(m_fd_w, -1, "open(w)")) return false;

    return true;
}

bool ControlInterface::process_events(std::string &magnet)
{
    int num_handlers = 1;
    struct pollfd fds[num_handlers];

    fds[0].events = POLLIN;
    fds[0].fd = m_fd_r;

    int ret = poll(fds, num_handlers, 100);

    if(this->handle_c_error(ret, -1, "poll()")) return false;

    if(ret == 0)
        return false;

    if(!(fds[0].revents & POLLIN))
        return false;

    fds[0].revents = 0;

    magnet = this->read_input_data();
    return true;
}

bool ControlInterface::handle_c_error(int ret, int fail_value, std::string subj)
{
    if(ret != fail_value)
        return false;

    std::cerr << "ERROR: ";

    if(subj.size() > 0)
        std::cerr << subj << ": ";

    std::cerr << strerror(errno) << std::endl;
    return true;
}

std::string ControlInterface::read_input_data()
{
    int buf_size = 2;
    char buf[buf_size];
    std::vector<char> data;

    int bytes_read;
    int total_bytes_read = 0;

    while((bytes_read = read(m_fd_r, buf, buf_size)) > 0) {
        if(data.size() < (data.size() + buf_size))
            data.resize(data.size() + buf_size);

        std::memcpy(data.data() + total_bytes_read, buf, buf_size);
        total_bytes_read += bytes_read;
    }

    return std::string(data.data());
}
