#ifndef TORRENTSESSION_H
#define TORRENTSESSION_H

#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/storage.hpp>
#include <libtorrent/allocator.hpp>
#include <libtorrent/storage_defs.hpp>
#include <libtorrent/alert_types.hpp>

#include <boost/system/error_code.hpp>
#include <boost/thread.hpp>
#include <nlohmann/json.hpp>
#include <ctime>
#include <thread>

#include "torrenthandle.h"
#include "storagememcache.h"
#include "controlinterface.h"

class TorrentSession
{
public:
    TorrentSession();
    ~TorrentSession();
    void run();
    void create(std::string params = std::string());

    bool stop_torent(TorrentHandle *h);
    bool t_handle_valid(TorrentHandle *h);

    TorrentHandle *add_magnet(std::string data);
    TorrentHandle *add_file(std::string data);

    int has_metadata(TorrentHandle *th);

    std::string get_metadata(TorrentHandle *h);

    nlohmann::json& get_params();



private:
    lt::session *m_session;
    StorageMemCache *m_cached_storage;
    ControlInterface *m_control_iface;
    nlohmann::json m_params;
    std::mutex m_torrent_handler_mutex, m_cleanup_mutex;
    std::vector<TorrentHandle*> m_torrent_handlers;

    void torrent_handle_thread(lt::session *session, std::string data, std::string k);
    void t_handle_add(TorrentHandle *h);
    void t_handle_remove(TorrentHandle *h);

    void parse_alert(lt::alert* general_alert);
    void alert_piece_finished(lt::piece_finished_alert *alert);
    void alert_torrent_finished(lt::torrent_finished_alert* alert);
    void alert_torrent_paused(libtorrent::torrent_paused_alert *alert);
    void alert_torrent_removed(libtorrent::torrent_removed_alert *alert);
    void alert_piece_read(lt::read_piece_alert *alert);
    void alert_metadata_received(lt::metadata_received_alert *alert);
    void alert_torrent_added(lt::torrent_added_alert *alert);

    bool no_alerts(std::vector<lt::alert*> alerts);

    TorrentHandle *t_handle_find(lt::torrent_handle &original_torrent_handle);
};

#endif // TORRENTSESSION_H
