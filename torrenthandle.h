#ifndef TORRENTHANDLE_H
#define TORRENTHANDLE_H

#include <string>
#include <stdio.h>

//#include "torrentsession.h"

#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/torrent_info.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/storage.hpp>
#include <libtorrent/allocator.hpp>
#include <libtorrent/storage_defs.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/file_storage.hpp>

#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp>
#include <nlohmann/json.hpp>

#include "storagememcache.h"
#include "filestreamer.h"

class TorrentHandle
{
public:
    TorrentHandle(nlohmann::json &params);
    ~TorrentHandle();
    lt::torrent_handle& get_torrent_handle();

    void add_magnet(session *session, std::string data);
    void add_file(session *session, std::string data);
    void prepare_add_torrent_params(lt::add_torrent_params &p);
    void piece_ready(lt::read_piece_alert* alert);
    void metadata_ready();

    bool is_my_torrent_handle(lt::torrent_handle &h);
    bool has_metadata();
    bool is_done();
    bool is_file_type_allowed(std::string his_type);

    std::string hash();
    std::string json_metadata();


    static std::string hash_to_string(lt::sha1_hash hash);

    // There are a few callbacks which should be filled from the python
    //
    // `stats_update`:
    // called on download progress
    void cb_set_stats_update(int (*cb)());
    void cb_call_stats_update();

    // `torrent_finished`:
    // called when all files of torrent are complete
    void cb_set_torrent_finished(int (*cb)());
    void cb_call_torrent_finished();

    // `file_finished`:
    // called when next file of torrent is done
    void cb_set_file_finished(int (*cb)());
    void cb_call_file_finished();

private:
    lt::torrent_handle m_handle;

    nlohmann::json m_params;

    bool m_metadata_ready;
    std::string m_hash_string, m_name;
    int m_num_files;
    int m_num_pieces;
    int m_piece_size;
    std::vector<FileStreamer*> m_files;

    static std::mutex cb_call_mutex;
    static std::mutex piece_ready_mutex;

    // Callback pointers
    // Should be set/called by cb_set_*() and cb_call_*() methods
    int (*f_cb_stats_update)();
    int (*f_cb_torrent_finished)();
    int (*f_cb_file_finished)();


    void prioritize_pieces(std::vector<int> &p);
    void free_file(FileStreamer *file);

};

#endif // TORRENTHANDLE_H
